import setuptools


configuration = {
    "name": "yavy",
    "version": "0.0.1",
    "description": "Distributed workflow management framework",
    "classifiers": [
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Topic :: System :: Distributed Computing"
    ],
    "keywords": "workflow task schedule distributed dependency management",
    "url": "https://bitbucket.org/thoughteer/yavy",
    "author": "Iskander Sitdikov",
    "author_email": "thoughteer@gmail.com",
    "license": "GPL",
    "packages": [
        "yavy",
        "yavy.lockers",
        "yavy.monitors",
        "yavy.registries",
        "yavy.requisites",
        "yavy.runners",
    ],
    "install_requires": [
        "fasteners >= 0.14.1",
        "kazoo >= 2.2.1",
        "networkx >= 1.10",
    ],
    "zip_safe": False
}
setuptools.setup(**configuration)

import inspect

from . import utilities
from .tagged import Tagged


class Context(Tagged):

    __GUID = "#wE0[~;e"

    def __init__(self, **kwargs):
        self.__entries = [
            attribute
            for _, attribute in inspect.getmembers(self.__class__)
            if isinstance(attribute, Entry)
        ]
        for entry in self.__entries:
            if entry.name in kwargs:
                entry.override(self, kwargs[entry.name])

    @property
    def __json__(self):
        return {
            "?": self.__GUID,
            "!": {
                self.__class__.__name__: {
                    entry.name: getattr(self, entry.name)
                    for entry in self.__entries
                }
            }
        }

    @utilities.constant
    def tag(self):
        return utilities.jsonize(self)


class Entry(utilities.constant):

    pass


def entry(evaluator):
    return Entry(evaluator)

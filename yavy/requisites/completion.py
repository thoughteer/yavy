from . import execution
from .. import context
from .. import exceptions
from .. import utilities
from ..requisite import Requisite
from .execution import ExecutionRequisite


class CompletionRequisite(Requisite):

    priority = ExecutionRequisite.priority - 1

    @context.entry
    def target(self):
        pass

    def satisfy(self, demander, workflow):
        utilities.wrap(demander.check, check_completed, target=self.target, workflow=workflow)


def check_completed(demander, state, target, workflow):
    if demander.check(state):
        return True
    if not workflow.environment.registry.get(target.tag)["yavy.complete"]:
        raise exceptions.RequisiteCheckError("%s not completed yet" % target)
    return False


def complete(target):
    return [
        execution.execute(target),
        CompletionRequisite(target=target)
    ]

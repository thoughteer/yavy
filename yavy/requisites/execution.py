from . import inclusion
from . import linkage
from .. import context
from .. import exceptions
from .. import utilities
from ..requisite import Requisite
from .binding import BindingRequisite


class ExecutionRequisite(Requisite):

    priority = BindingRequisite.priority - 1

    @context.entry
    def target(self):
        pass

    def satisfy(self, demander, workflow):
        utilities.wrap(demander.check, check_executed, target=self.target, workflow=workflow)


def check_executed(demander, state, target, workflow):
    if demander.check(state):
        return True
    if workflow.environment.registry.get(target.tag) is None:
        raise exceptions.RequisiteCheckError("%s not executed yet" % target)
    return False


def execute(target):
    return [
        inclusion.include(target),
        linkage.link(target),
        ExecutionRequisite(target=target)
    ]

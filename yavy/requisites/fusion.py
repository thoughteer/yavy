from .. import context
from ..requisite import Requisite


class FusionRequisite(Requisite):

    @context.entry
    def requisites(self):
        pass

    def satisfy(self, demander, workflow):
        for requisite in self.requisites:
            workflow.adjust(requisite, demander)


def fuse(requisites):
    return FusionRequisite(requisites=requisites)

from . import inclusion
from .. import context
from ..requisite import Requisite
from .assignment import AssignmentRequisite


class LinkageRequisite(Requisite):

    priority = AssignmentRequisite.priority - 1

    @context.entry
    def target(self):
        pass

    def satisfy(self, demander, workflow):
        workflow.tasks.link(self.target, demander)


def link(target):
    return [
        inclusion.include(target),
        LinkageRequisite(target=target)
    ]

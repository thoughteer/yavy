from .. import context
from ..requisite import Requisite


class InclusionRequisite(Requisite):

    @context.entry
    def target(self):
        pass

    def satisfy(self, demander, workflow):
        workflow.include(self.target)


def include(target):
    return [] if target is None else InclusionRequisite(target=target)

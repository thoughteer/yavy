from . import inclusion
from .. import context
from ..requisite import Requisite
from .linkage import LinkageRequisite


# TODO: enable arbitrary postponed requisite assignment
class WaitingRequisite(Requisite):

    priority = LinkageRequisite.priority - 1

    @context.entry
    def target(self):
        pass

    def satisfy(self, demander, workflow):
        for successor in workflow.tasks.successors(self.target):
            if successor == demander:
                continue
            workflow.adjust(successor, demander)


def wait(target):
    return [
        inclusion.include(target),
        WaitingRequisite(target=target)
    ]

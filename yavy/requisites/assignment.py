from . import inclusion
from .. import context
from ..requisite import Requisite
from .inclusion import InclusionRequisite


class AssignmentRequisite(Requisite):

    priority = InclusionRequisite.priority - 1

    @context.entry
    def assignee(self):
        pass

    @context.entry
    def requisite(self):
        pass

    def satisfy(self, demander, workflow):
        workflow.adjust(self.requisite, self.assignee)


def assign(requisite, assignee):
    return [
        inclusion.include(assignee),
        AssignmentRequisite(requisite=requisite, assignee=assignee)
    ]

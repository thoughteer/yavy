from . import inclusion
from .. import context
from .. import utilities
from ..requisite import Requisite
from .assignment import AssignmentRequisite


class BindingRequisite(Requisite):

    priority = AssignmentRequisite.priority - 1

    @context.entry
    def target(self):
        pass

    def satisfy(self, demander, workflow):
        utilities.wrap(demander.check, check_synchronized, target=self.target, workflow=workflow)


def bind(target):
    return [
        inclusion.include(target),
        BindingRequisite(target=target)
    ]


def check_synchronized(demander, state, target, workflow):
    if not demander.check(state):
        return False
    target_state = workflow.environment.registry.get(target.tag)
    if target_state is None:
        return True
    return target_state["yavy.timestamp"] <= state["yavy.timestamp"]

from .assignment import AssignmentRequisite


class AbstractionRequisite(AssignmentRequisite):

    assignee = None


def abstract(requisite):
    return AbstractionRequisite(requisite=requisite)

from .. import context
from .. import exceptions
from .. import utilities
from ..requisite import Requisite
from .assignment import AssignmentRequisite


class BlockageRequisite(Requisite):

    priority = AssignmentRequisite.priority - 1

    @context.entry
    def group(self):
        pass

    def satisfy(self, demander, workflow):
        utilities.wrap(demander.run, run_exclusively, group=self.group, workflow=workflow)


def block(group):
    return BlockageRequisite(group=group)


def run_exclusively(demander, state, group, workflow):
    try:
        with workflow.environment.lock(group, namespace="group"):
            return demander.run(state)
    except exceptions.LockAcquisitionError:
        raise exceptions.RequisiteCheckError("blocked by %s" % group)

from .context import Context


class Task(Context):

    def check(self, state):
        return state is not None and state["yavy.complete"]

    @property
    def requisite(self):
        return []

    def run(self, state):
        pass

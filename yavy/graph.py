import networkx
import networkx.algorithms.dag


# TODO: implement the topological sort without networkx
class Graph(object):

    def __contains__(self, node):
        return node in self._dag

    def __init__(self):
        self._dag = networkx.DiGraph()

    def __iter__(self):
        return iter(self._dag)

    def __len__(self):
        return len(self._dag)

    def add(self, node):
        self._dag.add_node(node)

    def find(self, exemplar):
        return next((node for node in self if node == exemplar), None)

    def link(self, source, target):
        self._dag.add_edge(source, target)

    def sort(self):
        return networkx.algorithms.dag.topological_sort(self._dag)

    def successors(self, node):
        return self._dag.successors(node)

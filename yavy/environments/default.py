from ..environment import Environment
from ..lockers.thread import ThreadLocker
from ..monitors.log import LogMonitor
from ..registries.process import ProcessRegistry


class DefaultEnvironment(Environment):

    def __init__(self):
        super(DefaultEnvironment, self).__init__(ThreadLocker(), ProcessRegistry(), LogMonitor())

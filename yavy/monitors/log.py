import logging

from ..monitor import Monitor


class LogMonitor(Monitor):

    logger = logging.getLogger(__name__)

    def update(self, task, stage, **kwargs):
        parameters = ", ".join(name + ": " + str(value) for name, value in kwargs.items())
        self.logger.info("%s %s (%s)", task, stage, parameters)

class Error(Exception):

    pass


class ExcusableError(Error):

    pass


class LockAcquisitionError(ExcusableError):

    def __init__(self, key):
        message = "Lock for key `%s` has been already acquired" % key
        super(LockAcquisitionError, self).__init__(message)


class RequisiteCheckError(ExcusableError):

    pass

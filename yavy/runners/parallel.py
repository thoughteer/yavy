import multiprocessing
import os
import signal
import sys

from ..runner import Runner


class ParallelRunner(Runner):

    def __call__(self):
        master_pid = os.getpid()
        slaves = [
            multiprocessing.Process(name=name, target=target)
            for name, target in self.__configuration.items()
        ]
        try:
            for slave in slaves:
                slave.start()
            for slave in slaves:
                slave.join()
        except BaseException as error:
            if os.getpid() == master_pid:
                for slave in slaves:
                    try:
                        slave.terminate()
                    except OSError:
                        pass
                for slave in slaves:
                    slave.join()
            raise error

    def __init__(self, configuration):
        self.__configuration = configuration
        signal.signal(signal.SIGTERM, lambda *args: sys.exit(1))

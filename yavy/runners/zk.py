import contextlib

from .managed import ManagedRunner


class ZKRunner(ManagedRunner):

    def __init__(self, target, zk, timeout=5):
        super(ZKRunner, self).__init__(target, zk_session(zk, timeout))


@contextlib.contextmanager
def zk_session(zk, timeout):
    zk.start(timeout=timeout)
    try:
        yield
    finally:
        zk.stop()
        zk.close()

import threading

from .. import utilities
from ..runner import Runner


class ThreadedRunner(Runner):

    def __call__(self):
        slaves = [
            threading.Thread(name=name, target=target)
            for name, target in self._configuration.items()
        ]
        try:
            for slave in slaves:
                slave.start()
        finally:
            running = True
            while running:
                try:
                    for slave in slaves:
                        slave.join(timeout=0.1)
                    running = any(slave.is_alive() for slave in slaves)
                except BaseException as error:
                    unnotified_slaves = set(slaves)
                    while unnotified_slaves:
                        notified_slaves = set()
                        for slave in unnotified_slaves:
                            try:
                                utilities.raise_exception(error.__class__, slave)
                                notified_slaves.add(slave)
                            except:
                                pass
                        unnotified_slaves -= notified_slaves
                        alive_slaves = set(slave for slave in slaves if slave.is_alive())
                        unnotified_slaves &= alive_slaves

    def __init__(self, target, prefix="T", count=1):
        self._configuration = {
            "%s#%d" % (prefix, index + 1): target
            for index in range(count)
        }

from ..runner import Runner


class ManagedRunner(Runner):

    def __call__(self):
        with self.__manager:
            self.__target()

    def __init__(self, target, manager):
        self.__target = target
        self.__manager = manager

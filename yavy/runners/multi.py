from .parallel import ParallelRunner


class MultiRunner(ParallelRunner):

    def __init__(self, target, prefix="P", count=1):
        configuration = {
            "%s#%d" % (prefix, index + 1): target
            for index in range(count)
        }
        super(MultiRunner, self).__init__(configuration)

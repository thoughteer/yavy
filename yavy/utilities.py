import contextlib
import ctypes
import functools
import hashlib
import inspect
import json
import threading


# TODO: use `functools.wraps` in wrappers.


class constant(property):

    def __init__(self, evaluator):
        super(constant, self).__init__(fget=self._get)
        self.__evaluator = evaluator
        self.__cache = "__cached__%s" % evaluator.__name__

    def _get(self, owner):
        if not hasattr(owner, self.__cache):
            self.override(owner, self.__evaluator(owner))
        return getattr(owner, self.__cache)

    @property
    def name(self):
        return self.__evaluator.__name__

    def override(self, owner, value):
        setattr(owner, self.__cache, value)


def deduplicated(procedure):

    def deduplicated_procedure(*args, **kwargs):
        if tuple(args) in deduplicated_procedure.__calls:
            return
        deduplicated_procedure.__calls.add(tuple(args))
        return procedure(*args, **kwargs)

    deduplicated_procedure.__calls = set()
    return deduplicated_procedure


def find_main_thread():
    return next(t for t in threading.enumerate() if t.name == "MainThread")


def jsonize(o):

    class JSONEncoder(json.JSONEncoder):

        def default(self, o):
            if hasattr(o, "__json__"):
                return o.__json__
            return super(JSONEncoder, self).default(o)

    return JSONEncoder(sort_keys=True, separators=(",", ":")).encode(o)


def raise_exception(Class, thread):
    if not inspect.isclass(Class):
        raise TypeError("only types can be raised (not instances)")
    if not thread.is_alive():
        return
    if hasattr(thread, "_thread_id"):
        thread_id = thread._thread_id
    else:
        thread_id = None
        for i, o in threading._active.items():
            if o is thread:
                thread_id = ctypes.c_long(i)
        if thread_id is None:
            raise threading.ThreadError("cannot determine thread's id")
    result = ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, ctypes.py_object(Class))
    if result == 0:
        raise ValueError("invalid thread id")
    elif result != 1:
        ctypes.pythonapi.PyThreadState_SetAsyncExc(thread_id, None)
        raise SystemError("PyThreadState_SetAsyncExc failed")


def sha1(string):
    return hashlib.sha1(string.encode("utf-8")).hexdigest()


def wrap(method, function, **parameters):

    @contextlib.contextmanager
    def method_substitution(owner):
        original = getattr(owner, method.__name__)
        setattr(owner, method.__name__, method)
        try:
            yield
        finally:
            setattr(owner, method.__name__, original)

    def wrapped_method(*args, **kwargs):
        owner = method.__self__
        reduced_function = functools.partial(function, **parameters)
        with method_substitution(owner):
            return reduced_function(owner, *args, **kwargs)

    wrapped_method.__self__ = method.__self__
    wrapped_method.__name__ = method.__name__
    setattr(method.__self__, method.__name__, wrapped_method)

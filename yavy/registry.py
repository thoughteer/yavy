class Registry(object):

    def get(self, key):
        raise NotImplementedError

    def put(self, key, value):
        raise NotImplementedError

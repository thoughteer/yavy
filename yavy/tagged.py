class Tagged(object):

    def __eq__(self, other):
        return other is not None and other.tag == self.tag

    def __hash__(self):
        return hash(self.tag)

    def __ne__(self, other):
        return not self.__eq__(other)

    def __repr__(self):
        return self.tag

    @property
    def tag(self):
        raise NotImplementedError

import copy
import logging
import datetime

from . import exceptions
from .environments.default import DefaultEnvironment
from .graph import Graph
from .heap import Heap
from .task import Task


class Workflow(object):

    logger = logging.getLogger(__name__)

    def __init__(self, objective, environment=DefaultEnvironment()):
        self.environment = environment
        self.tasks = Graph()
        self.requisites = Heap()
        self.include(objective)
        self.expand()
        self.queue = self.tasks.sort()

    def __register_state(self, task, state, complete):
        # TODO: move `get current UTC timestamp` to utilities.
        state = self.__update_state(state, {
            "yavy.timestamp": datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S,%fZ"),
            "yavy.complete": complete,
        })
        self.environment.registry.put(task.tag, state)

    def __update_state(self, state, delta):
        if delta is None:
            return state
        state = {} if state is None else state
        state.update(delta)
        return state

    def _run_task(self, task):
        self.logger.debug("Selected task %s", task)
        try:
            with self.environment.lock(task.tag, namespace="task"):
                self.logger.debug("Acquired the lock")
                state = self.environment.registry.get(task.tag)
                self.logger.debug("Got the state: %s", state)
                try:
                    skip = task.check(state)
                except Exception as error:
                    self.environment.monitor.update(task, "pending", reason=error)
                    raise
                finally:
                    self.logger.debug("Checked")
                if skip:
                    self.logger.debug("Skipped")
                    return
                self.environment.monitor.update(task, "running")
                state = copy.deepcopy(state)
                try:
                    delta = task.run(state)
                    state = self.__update_state(state, delta)
                except Exception as error:
                    self.__register_state(task, state, complete=False)
                    self.logger.debug("Registered failure: %s", state)
                    self.environment.monitor.update(task, "failed", reason=error)
                    raise
                else:
                    self.__register_state(task, state, complete=True)
                    self.logger.debug("Registered success: %s", state)
                    self.environment.monitor.update(task, "completed", state=state)
        except exceptions.ExcusableError as error:
            self.logger.debug("Raised %s: %s", error.__class__.__name__, error.message)
        except Exception:
            self.logger.exception("Aborted")

    def adjust(self, requisite, demander=None):
        requisite = unify(requisite)
        demander = self.tasks.find(demander)
        self.requisites.push((requisite, demander), requisite.priority)

    def expand(self):
        while self.requisites:
            requisite, demander = self.requisites.pop()
            requisite.satisfy(demander, self)

    def include(self, task):
        if task is None or task in self.tasks:
            return
        self.tasks.add(task)
        self.adjust(task.requisite, task)

    def run(self):
        for task in self.queue:
            self._run_task(task)


def unify(requisite):
    from . import requisites
    if isinstance(requisite, list):
        return requisites.fuse(requisite)
    elif isinstance(requisite, Task):
        return unify(requisites.complete(requisite))
    return requisite

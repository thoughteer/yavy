import contextlib

from .. import exceptions
from ..locker import Locker


class ThreadLocker(Locker):

    def __init__(self):
        self._locked_keys = set()

    @contextlib.contextmanager
    def lock(self, key):
        if key in self._locked_keys:
            raise exceptions.LockAcquisitionError(key)
        self._locked_keys.add(key)
        try:
            yield
        finally:
            self._locked_keys.remove(key)

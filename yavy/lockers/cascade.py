import contextlib

from ..locker import Locker


class CascadeLocker(Locker):

    def __init__(self, *stages):
        self.__stages = stages

    def lock(self, key):
        return lock([stage.lock(key) for stage in self.__stages])


@contextlib.contextmanager
def lock(sublocks):
    if not sublocks:
        yield
        return
    with sublocks[0], lock(sublocks[1:]):
        yield

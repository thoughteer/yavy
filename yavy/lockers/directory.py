import collections
import contextlib
import os

import fasteners

from .. import exceptions
from .. import utilities
from ..locker import Locker
from .cascade import CascadeLocker
from .process import ProcessLocker


class DirectoryLocker(CascadeLocker):

    _process_lockers = collections.defaultdict(ProcessLocker)

    def __init__(self, directory):
        directory = os.path.abspath(str(directory))
        super(DirectoryLocker, self).__init__(
                self._process_lockers[directory],
                ThreadUnsafeDirectoryLocker(directory))


class ThreadUnsafeDirectoryLocker(Locker):

    def __init__(self, directory):
        self._directory = directory

    @contextlib.contextmanager
    def lock(self, key):
        file_lock_path = os.path.join(self._directory, utilities.sha1(key))
        file_lock = fasteners.InterProcessLock(file_lock_path)
        if not file_lock.acquire(blocking=False):
            raise exceptions.LockAcquisitionError(key)
        try:
            yield
        finally:
            file_lock.release()

import contextlib
import threading

from .. import exceptions
from ..locker import Locker


class ProcessLocker(Locker):

    def __init__(self):
        self._locked_keys = set()
        self._mutex = threading.Lock()

    @contextlib.contextmanager
    def lock(self, key):
        with self._mutex:
            if key in self._locked_keys:
                raise exceptions.LockAcquisitionError(key)
            self._locked_keys.add(key)
        try:
            yield
        finally:
            with self._mutex:
                self._locked_keys.remove(key)

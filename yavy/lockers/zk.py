import contextlib
import threading

import kazoo.client
import kazoo.exceptions

from .. import exceptions
from .. import utilities
from ..locker import Locker


class ZKLocker(Locker):

    def __init__(self, zk, znode):
        self._zk = zk
        self._znode = znode
        self._mutex = threading.Lock()
        self._counter = 0

    def handle(self, state):
        if state == kazoo.client.KazooState.SUSPENDED:
            main_thread = utilities.find_main_thread()
            utilities.raise_exception(kazoo.exceptions.ZookeeperError, main_thread)

    @contextlib.contextmanager
    def lock(self, key):
        with self._mutex:
            if self._counter == 0:
                self._zk.add_listener(self.handle)
            self._counter += 1
        try:
            path = self._znode + "/" + utilities.sha1(key)
            try:
                self._zk.create(path, ephemeral=True, makepath=True)
            except kazoo.exceptions.NodeExistsError:
                raise exceptions.LockAcquisitionError(key)
            try:
                yield
            finally:
                try:
                    self._zk.delete(path)
                except kazoo.exceptions.ZookeeperError:
                    pass
        finally:
            with self._mutex:
                self._counter -= 1
                if self._counter == 0:
                    self._zk.remove_listener(self.handle)

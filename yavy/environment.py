class Environment(object):

    def __init__(self, locker, registry, monitor):
        self.locker = locker
        self.registry = registry
        self.monitor = monitor

    def lock(self, key, namespace=""):
        return self.locker.lock("%s/%s" % (namespace, key))

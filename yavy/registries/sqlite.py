import json
import sqlite3
import threading

from ..registry import Registry


_CREATION_QUERY = "CREATE TABLE IF NOT EXISTS %s (key TEXT UNIQUE, value TEXT)"
_SELECTION_QUERY = "SELECT * FROM %s WHERE key = ?"
_INSERTION_QUERY = "INSERT OR REPLACE INTO %s VALUES (?, ?)"


class SQLiteRegistry(Registry):

    def __enter__(self):
        self._local.client = sqlite3.connect(self._database)
        self._local.client.execute(_CREATION_QUERY % self._table)
        self._local.client.commit()
        return self

    def __exit__(self, *args):
        self._local.client.close()

    def __init__(self, database, table="registry"):
        self._database = database
        self._table = table
        self._local = threading.local()

    def get(self, key):
        record = self._local.client.execute(_SELECTION_QUERY % self._table, (key,)).fetchone()
        if record is None:
            return None
        return json.loads(record[1])

    def put(self, key, value):
        self._local.client.execute(_INSERTION_QUERY % self._table, (key, json.dumps(value)))
        self._local.client.commit()

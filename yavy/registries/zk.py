import json

import kazoo.exceptions

from .. import utilities
from ..registry import Registry


class ZKRegistry(Registry):

    def __init__(self, zk, znode):
        self._zk = zk
        self._znode = znode

    def _get_path(self, key):
        return self._znode + "/" + utilities.sha1(key)

    def get(self, key):
        try:
            return json.loads(self._zk.get(self._get_path(key))[0])["value"]
        except kazoo.exceptions.NoNodeError:
            return None

    def put(self, key, value):
        path = self._get_path(key)
        data = json.dumps({"key": key, "value": value})
        try:
            self._zk.set(path, data)
        except kazoo.exceptions.NoNodeError:
            self._zk.create(path, data, makepath=True)

import heapq

from . import utilities


class Heap(object):

    def __init__(self):
        self._items = []

    def __len__(self):
        return len(self._items)

    def pop(self):
        return heapq.heappop(self._items)[2]

    @utilities.deduplicated
    def push(self, item, priority=0):
        heapq.heappush(self._items, (-priority, len(self), item))

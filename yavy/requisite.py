from .context import Context


class Requisite(Context):

    priority = 0

    def satisfy(self, demander, workflow):
        raise NotImplementedError

#### Task
- declares a unique key (could be derived implicitly from its context entries)
- declares a requisite that adjusts the workflow so that the task can run properly
- checks its last state (recorded in a registry) and
    - returns True if the task is complete and does not need to be run
    - returns False if the task is incomplete and can be run
    - raises an exception if the task is incomplete but cannot be run
- runs some code and returns its new state (JSON-object or None) assuming
    - its requisite is satisfied
    - it is the only task instance with this key currently running

#### Workflow
- expands an objective task into a DAG of tasks by satisfying its requisite
- performs topological sorting of the DAG
- runs all the tasks in correct order
    - uses a locker to resolve conflicts between instances of the same task
    - uses a registry to track their states
    - uses a monitor to report the current stage

#### Requisite
- applies to a pair (demander: Task, workflow: Workflow)
- adjusts the workflow for the demander
    - can modify the requisite heap of the workflow
    - can modify the DAG of the workflow
    - can change the behaviour of any tasks in the DAG
- can be safely re-used and combined with other requisites
- is satisfied in order, according to its priority
- by default
    - any yavy.Task T transforms into complete(T)
    - any list L transforms into fuse(L)

#### Locker
- allows to acquire a lock for a specific key (string)
- no two clients can acquire locks for the same key simultaneously
- can be combined with other lockers to speed up the acquisition

#### Registry
- stores (key, state) pairs, where
    - key is the key of a task
    - state is a JSON-object holding the result of the last execution of the task (or None)
        - yavy.timestamp holds a UTC time of the last execution
        - yavy.complete indicates whether the last execution completed successfully
- allows to fetch states by key and to set up a new state for a given key
- handles concurrent access issues

#### Monitor
- reports the stage a task is currently at, as well as additional information

#### Context
- allows to define parameters (called entries) consisting of
    - a name
    - an optional description
    - a way to evaluate its default value based on other entries
- provides parameter-based initialization
    - you can specify some context entries in the constructor
    - other entries will be evaluated automagically
- can be represented as a string (via its `key` property)
    - which is unique for the given context class and the values of its entries
- serves as a base class for every Task
    - so no need to define the key explicitly

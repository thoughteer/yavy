import pytest

import yavy


GRAPHS = [
    {},
    {"node": []},
    {2: [], 1: [2], 0: [1, 2]},
    {"cycle": [], 100500: [], ("1", None): [2], 2: [("1", None), 100500]}
]


@pytest.fixture
def empty_graph():
    return yavy.Graph()


@pytest.fixture(params=GRAPHS)
def graph(request):
    result = yavy.Graph()
    for node in request.param:
        result.add(node)
    for node in request.param:
        for successor in request.param[node]:
            result.link(node, successor)
    result.__nodes = request.param
    return result

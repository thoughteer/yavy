import mock
import pytest


@pytest.fixture
def locker_cascade():
    return [mock.MagicMock(name=name) for name in ["a", "b", "c"]]

import pytest

import yavy
import yavy.context


class BaseContext(yavy.Context):

    @yavy.context.entry
    def required_entry(self):
        raise NotImplementedError

    @yavy.context.entry
    def evaluated_entry(self):
        return "based on " + self.required_entry


class DerivedContext(BaseContext):

    @yavy.context.entry
    def required_entry(self):
        return "no longer required"


def test_invalid_base_context_initialization():
    with pytest.raises(NotImplementedError):
        BaseContext().evaluated_entry


def test_partial_base_context_initialization():
    base_context = BaseContext(required_entry="X")
    assert base_context.required_entry == "X"
    assert base_context.evaluated_entry == "based on X"


def test_complete_base_context_initialization():
    base_context = BaseContext(required_entry="X", evaluated_entry="Y")
    assert base_context.required_entry == "X"
    assert base_context.evaluated_entry == "Y"


def test_default_derived_context_initialization():
    derived_context = DerivedContext()
    assert derived_context.required_entry == "no longer required"
    assert derived_context.evaluated_entry == "based on no longer required"


def test_partial_derived_context_initialization():
    derived_context = DerivedContext(required_entry="X")
    assert derived_context.required_entry == "X"
    assert derived_context.evaluated_entry == "based on X"


def test_complete_derived_context_initialization():
    derived_context = DerivedContext(required_entry="X", evaluated_entry="Y")
    assert derived_context.required_entry == "X"
    assert derived_context.evaluated_entry == "Y"


def test_context_entry_access():
    assert isinstance(BaseContext.required_entry, yavy.context.Entry)
    assert isinstance(BaseContext.evaluated_entry, yavy.context.Entry)
    assert BaseContext.required_entry.name == "required_entry"
    assert BaseContext.evaluated_entry.name == "evaluated_entry"


def test_context_representation_invariance():
    assert repr(DerivedContext()) == repr(DerivedContext())


def test_context_class_differentiation():
    sample_context = DerivedContext()
    similar_context = BaseContext(required_entry="no longer required")
    assert repr(sample_context) != repr(similar_context)


def test_context_data_differentiation():
    default_context = DerivedContext()
    custom_context = DerivedContext(required_entry="custom")
    assert repr(default_context) != repr(custom_context)

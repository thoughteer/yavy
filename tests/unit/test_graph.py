import copy

import pytest


def test_initialization(empty_graph):
    assert not empty_graph


def test_adding_nodes(empty_graph):
    graph = empty_graph
    for i in range(1, 6):
        graph.add(i)
        assert len(graph) == i


def test_linking_nodes(empty_graph):
    graph = empty_graph
    graph.add("1")
    graph.add("2")
    graph.link("1", "2")
    assert graph.successors("1") == ["2"]
    assert graph.successors("2") == []


def test_checking_membership(graph):
    for node in graph.__nodes:
        assert node in graph
    assert "outlier" not in graph


def test_iterating_over_nodes(graph):
    assert len(set(graph)) == len(graph)
    assert set(graph) == set(graph.__nodes)


def test_finding_nodes(graph):
    for node in graph.__nodes:
        assert graph.find(copy.copy(node)) is node


def test_getting_successors(graph):
    for node in graph.__nodes:
        successors = graph.successors(node)
        assert len(successors) == len(graph.__nodes[node])
        assert set(successors) == set(graph.__nodes[node])


def test_sorting(graph):
    if "cycle" in graph:
        with pytest.raises(Exception):
            graph.sort()
        return
    queue = graph.sort()
    assert len(queue) == len(graph)
    assert set(queue) == set(graph)
    for index, node in enumerate(queue):
        assert set(graph.successors(node)).isdisjoint(queue[:index])

import threading

import pytest

from yavy import exceptions
from yavy import lockers


def _check_locker(process_locker, key, entry_flag=None, exit_flag=None):
    with process_locker.lock(key):
        if entry_flag is not None:
            entry_flag.set()
        if exit_flag is not None:
            exit_flag.wait()


def test_single_lock_acquisition():
    process_locker = lockers.ProcessLocker()
    _check_locker(process_locker, "key")


def _spawn_thread(process_locker, key, active=False):
    args = (process_locker, key)
    if active:
        entry_flag = threading.Event()
        exit_flag = threading.Event()
        args += (entry_flag, exit_flag)
    thread = threading.Thread(target=_check_locker, args=args)
    if active:
        return thread, entry_flag, exit_flag
    return thread


def test_double_lock_acquisition():
    process_locker = lockers.ProcessLocker()
    thread, entry_flag, exit_flag = _spawn_thread(process_locker, "key", True)
    try:
        thread.start()
        entry_flag.wait()
        with pytest.raises(exceptions.LockAcquisitionError):
            _check_locker(process_locker, "key")
    finally:
        exit_flag.set()
        thread.join()


def test_consecutive_lock_acquisition():
    process_locker = lockers.ProcessLocker()
    thread = _spawn_thread(process_locker, "key")
    thread.start()
    thread.join()
    _check_locker(process_locker, "key")


def test_independent_lock_acquisition():
    process_locker = lockers.ProcessLocker()
    thread, entry_flag, exit_flag = _spawn_thread(process_locker, "key#1", True)
    try:
        thread.start()
        entry_flag.wait()
        _check_locker(process_locker, "key#2")
    finally:
        exit_flag.set()
        thread.join()

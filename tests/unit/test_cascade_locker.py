import mock
import pytest

import yavy.exceptions
import yavy.lockers


def test_single_lock_acquisition(locker_cascade):
    cascade_locker = yavy.lockers.CascadeLocker(*locker_cascade)
    with cascade_locker.lock("key"):
        pass
    for locker in locker_cascade:
        assert locker.lock.return_value.mock_calls == [
            mock.call.__enter__(),
            mock.call.__exit__(None, None, None)
        ]


def test_double_lock_acquisition(locker_cascade):
    cascade_locker = yavy.lockers.CascadeLocker(*locker_cascade)
    with cascade_locker.lock("key"):
        locker_cascade[-1].lock.return_value.__enter__.side_effect = yavy.exceptions.LockAcquisitionError("key")
        same_cascade_locker = yavy.lockers.CascadeLocker(*locker_cascade)
        with pytest.raises(yavy.exceptions.LockAcquisitionError):
            with same_cascade_locker.lock("key"):
                pass
        for locker in locker_cascade[:-1]:
            assert locker.lock.return_value.__exit__.called

import pytest

from yavy import exceptions
from yavy import lockers


def test_single_lock_acquisition():
    thread_locker = lockers.ThreadLocker()
    with thread_locker.lock("key"):
        pass


def test_double_lock_acquisition():
    thread_locker = lockers.ThreadLocker()
    with thread_locker.lock("key"):
        with pytest.raises(exceptions.LockAcquisitionError):
            with thread_locker.lock("key"):
                pass


def test_consecutive_lock_acquisition():
    thread_locker = lockers.ThreadLocker()
    with thread_locker.lock("key"):
        pass
    with thread_locker.lock("key"):
        pass


def test_independent_lock_acquisition():
    thread_locker = lockers.ThreadLocker()
    with thread_locker.lock("key#1"):
        with thread_locker.lock("key#2"):
            pass

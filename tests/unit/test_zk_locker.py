import kazoo.exceptions
import pytest

from yavy import exceptions
from yavy import lockers


class KazooClientMock(object):

    def __init__(self, fail_on_delete=False):
        self._paths = set()
        self._fail_on_delete = fail_on_delete

    def add_listener(self, listener):
        pass

    def create(self, path, ephemeral=False, makepath=False):
        if path in self._paths:
            raise kazoo.exceptions.NodeExistsError
        self._paths.add(path)

    def delete(self, path):
        self._paths.remove(path)
        if self._fail_on_delete:
            raise kazoo.exceptions.ZookeeperError

    def remove_listener(self, listener):
        pass

    def start(self, timeout):
        pass


@pytest.fixture(params=[False, True])
def zk(request):
    return KazooClientMock(fail_on_delete=request.param)


def test_single_lock_acquisition(zk):
    zk_locker = lockers.ZKLocker(zk, "/yavy/test/node")
    with zk_locker.lock("key"):
        pass


def test_double_lock_acquisition(zk):
    zk_locker = lockers.ZKLocker(zk, "/yavy/test/node")
    same_zk_locker = lockers.ZKLocker(zk, "/yavy/test/node")
    with zk_locker.lock("key"):
        with pytest.raises(exceptions.LockAcquisitionError):
            with same_zk_locker.lock("key"):
                pass


def test_consecutive_lock_acquisition(zk):
    zk_locker = lockers.ZKLocker(zk, "/yavy/test/node")
    with zk_locker.lock("key"):
        pass
    with zk_locker.lock("key"):
        pass


def test_independent_lock_acquisition(zk):
    zk_locker = lockers.ZKLocker(zk, "/yavy/test/node#1")
    another_zk_locker = lockers.ZKLocker(zk, "/yavy/test/node#2")
    with zk_locker.lock("key#1"):
        with zk_locker.lock("key#2"):
            with another_zk_locker.lock("key#2"):
                pass

import multiprocessing

import pytest

from yavy import exceptions
from yavy import lockers


def _check_locker(directory_locker, key, entry_flag=None, exit_flag=None):
    with directory_locker.lock(key):
        if entry_flag is not None:
            entry_flag.set()
        if exit_flag is not None:
            exit_flag.wait()


def test_single_lock_acquisition(tmpdir):
    directory_locker = lockers.DirectoryLocker(tmpdir)
    _check_locker(directory_locker, "key")


def _spawn_process(directory_locker, key, active=False):
    args = (directory_locker, key)
    if active:
        entry_flag = multiprocessing.Event()
        exit_flag = multiprocessing.Event()
        args += (entry_flag, exit_flag)
    process = multiprocessing.Process(target=_check_locker, args=args)
    if active:
        return process, entry_flag, exit_flag
    return process


def test_double_lock_acquisition(tmpdir):
    directory_locker = lockers.DirectoryLocker(tmpdir)
    same_directory_locker = lockers.DirectoryLocker(tmpdir)
    process, entry_flag, exit_flag = _spawn_process(directory_locker, "key", True)
    try:
        process.start()
        entry_flag.wait()
        with pytest.raises(exceptions.LockAcquisitionError):
            _check_locker(same_directory_locker, "key")
    finally:
        exit_flag.set()
        process.join()


def test_consecutive_lock_acquisition(tmpdir):
    directory_locker = lockers.DirectoryLocker(tmpdir)
    process = _spawn_process(directory_locker, "key")
    process.start()
    process.join()
    _check_locker(directory_locker, "key")


def test_independent_lock_acquisition(tmpdir):
    directory_locker = lockers.DirectoryLocker(tmpdir.mkdir("sub#1"))
    another_directory_locker = lockers.DirectoryLocker(tmpdir.mkdir("sub#2"))
    process, entry_flag, exit_flag = _spawn_process(directory_locker, "key#1", True)
    try:
        process.start()
        entry_flag.wait()
        with directory_locker.lock("key#2"):
            _check_locker(another_directory_locker, "key#2")
    finally:
        exit_flag.set()
        process.join()

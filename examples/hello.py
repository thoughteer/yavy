import functools
import logging
import os
import shutil
import time

import kazoo.client

import yavy
import yavy.context
import yavy.exceptions
import yavy.lockers
import yavy.monitors
import yavy.registries
import yavy.requisites
import yavy.runners


logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(processName)s/%(threadName)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)

logging.getLogger("kazoo").setLevel(logging.INFO)


class CreateHello(yavy.Task):

    index = 0

    @yavy.context.entry
    def test_argument(self):
        return (0, {1: [None, True, False, {}, "string"]})

    @property
    def requisite(self):
        return [
            yavy.requisites.assign(self, CopyHello()),
            yavy.requisites.assign(yavy.requisites.bind(self), RemoveHello()),
            yavy.requisites.assign(yavy.requisites.execute(self), RemoveHello()),
            yavy.requisites.assign(yavy.requisites.wait(self), RemoveHello()),
            yavy.requisites.block("hello"),
        ]

    def load(self):
        try:
            with open("hello", "r") as stream:
                return stream.readline().strip()
        except Exception:
            return None

    def check(self, state):
        return state is not None and self.load() == "3"

    def run(self, state):
        for i in range(3):
            time.sleep(1)
            import math
            s = 0
            for j in range(100000):
                s += math.sqrt(j)
        value = self.load()
        value = int(value) + 1 if value is not None else 1
        with open("hello", "w") as stream:
            stream.write("{}".format(value))


class CopyHello(yavy.Task):

    @property
    def requisite(self):
        return yavy.requisites.block("hello")

    def run(self, state):
        time.sleep(1)
        shutil.copy("hello", "copy")


class DisplayHello(yavy.Task):

    @property
    def requisite(self):
        return [
            yavy.requisites.bind(CreateHello()),
            CreateHello(),
        ]

    def run(self, state):
        time.sleep(1)
        with open("hello", "r") as stream:
            line = stream.read()
        print("> " + line)


class RemoveHello(yavy.Task):

    def load(self):
        try:
            with open("hello", "r") as stream:
                return stream.readline().strip()
        except Exception:
            return None

    def check(self, state):
        if self.load() != "3":
            raise yavy.exceptions.ExcusableError("bad time to remove files")
        super(RemoveHello, self).check(state)

    def run(self, state):
        time.sleep(1)
        os.remove("hello")
        assert not os.path.exists("hello")


class DoAllStuff(yavy.Task):

    @property
    def requisite(self):
        return [DisplayHello(), RemoveHello()]


def main():
    objective = DoAllStuff()
    zk = kazoo.client.KazooClient()
    locker = yavy.lockers.ZKLocker(zk, "yavy/locker")
    registry = yavy.registries.SQLiteRegistry("registry.db")
    monitor = yavy.monitors.LogMonitor()
    environment = yavy.Environment(locker, registry, monitor)
    workflow = yavy.Workflow(objective, environment)

    @functools.partial(yavy.runners.MultiRunner, count=5)
    @functools.partial(yavy.runners.ZKRunner, zk=zk)
    @functools.partial(yavy.runners.ThreadedRunner, count=3)
    @functools.partial(yavy.runners.ManagedRunner, manager=registry)
    def run():
        workflow.run()

    for _ in range(3):
        run()

    try:
        os.remove("copy")
    except:
        pass


if __name__ == "__main__":
    main()
